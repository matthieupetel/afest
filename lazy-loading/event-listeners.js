(function () {

    // IMAGES
    let images = [].slice.call(document.querySelectorAll('img.placeholder'));
    let active = false;

    const lazyLoad = () => {
        if (active === false) {
            active = true;

            setTimeout(() => {
                images.forEach((image) => {
                    if ((image.getBoundingClientRect().top <= window.innerHeight && image.getBoundingClientRect().bottom >= 0)
                        && getComputedStyle(image).display !== 'none') {
                        image.src = image.dataset.src;

                        // Update classes when image is fully loaded
                        image.addEventListener('load', () => {
                            image.classList.remove('placeholder');
                            image.classList.add('loaded');
                        });

                        // Remove loaded image from array of images to lazy load
                        images = images.filter((img) => {
                            return img !== image;
                        });

                        // remove event listeners when there is no images left to lazy load
                        if (images.length === 0) {
                            document.removeEventListener('scroll', lazyLoad);
                            window.removeEventListener('resize', lazyLoad);
                            window.removeEventListener('orientationchange', lazyLoad);
                        }
                    }
                });

                active = false;
            }, 200);
        }
    };

    document.addEventListener('scroll', lazyLoad);
    window.addEventListener('resize', lazyLoad);
    window.addEventListener('orientationchange', lazyLoad);



    // IFRAMES
    let iframes = [].slice.call(document.querySelectorAll('iframe.lazy'));

    const lazyLoadIframes = () => {
        if (active === false) {
            active = true;

            setTimeout(() => {
                iframes.forEach((iframe) => {
                    if ((iframe.getBoundingClientRect().top <= window.innerHeight && iframe.getBoundingClientRect().bottom >= 0)
                        && getComputedStyle(iframe).display !== 'none') {
                        iframe.src = iframe.dataset.src;

                        // Update classes when image is fully loaded
                        iframe.classList.remove('lazy');
                        iframe.classList.add('loaded');

                        // Remove loaded iframe from array of iframes to lazy load
                        iframes = iframes.filter((element) => {
                            return element !== iframe;
                        });

                        // Remove event listeners when there is no iframes left to lazy load
                        if (iframes.length === 0) {
                            document.removeEventListener('scroll', lazyLoad);
                            window.removeEventListener('resize', lazyLoad);
                            window.removeEventListener('orientationchange', lazyLoad);
                        }
                    }
                });

                active = false;
            }, 200);
        }
    };

    document.addEventListener('scroll', lazyLoadIframes);
    window.addEventListener('resize', lazyLoadIframes);
    window.addEventListener('orientationchange', lazyLoadIframes);
})();
