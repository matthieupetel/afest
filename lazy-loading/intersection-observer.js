(function () {
    // IMAGES
    const images = document.querySelectorAll('img.placeholder');

    const io = new IntersectionObserver(
        (entries) => {
            for (const entry of entries) {
                console.dir(entry);

                if (entry.intersectionRatio > 0) {
                    // Replace source to actually load the wanted image
                    entry.target.src = entry.target.dataset.src;

                    // Update classes when image is fully loaded
                    entry.target.addEventListener('load', () => {
                        entry.target.classList.remove('placeholder');
                        entry.target.classList.add('loaded');
                    });

                    // Some available data
                    console.log(`entry.isIntersecting ${entry.isIntersecting}`);
                    console.log(`entry.isVisible ${entry.isVisible}`);
                    console.log(`entry.intersectionRatio ${entry.intersectionRatio}`);

                    // When done, unobserve the image
                    io.unobserve(entry.target);
                }
            }
        },
        {
            // Default: document root
            root: null,

            // Optionally set a margin to trigger event before reaching the element edges
            rootMargin: "200px",

            // Set more threshold values to enable more event triggering
            // 0 (default) = when any part becomes visible
            // 0.25 = when 25% becomes visible
            // 0.5 = when 50% becomes visible
            // 0.75 = when 75% becomes visible
            // 1 = when element becomes fully visible
            threshold: [0, 0.25, 0.5, 0.75, 1],
        }
    );


    // Start observing an element
    // io.observe(images);

    // Stop observing an element
    // io.unobserve(element);

    // Disable entire IntersectionObserver
    // io.disconnect();

    if (images.length > 0) {
        for (let image of images) {
            io.observe(image);
        }
    }



    // IFRAMES

    const iframes = document.querySelectorAll('iframe.lazy');

    const ioIframes = new IntersectionObserver(
        (entries) => {
            for (const entry of entries) {
                console.dir(entry);

                if (entry.intersectionRatio > 0) {
                    // Replace source to actually load the wanted iframe
                    entry.target.src = entry.target.dataset.src;

                    // Update classes when iframe is fully loaded
                    entry.target.classList.remove('lazy');
                    entry.target.classList.add('loaded');

                    // When done, unobserve the iframe
                    io.unobserve(entry.target);
                }
            }
        }
    );

    if (iframes.length > 0) {
        for (let iframe of iframes) {
            ioIframes.observe(iframe);
        }
    }
})();
